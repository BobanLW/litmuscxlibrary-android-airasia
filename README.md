# LitmusCXLibrary #

## Installation

LitmusCXLibrary is available through [Jcenter](https://bintray.com/bintray/jcenter). To install
it, simply add the following line to your build.gradle file under dependencies:

```ruby
compile 'com.litmusworld.litmuscxlibrary:litmuscxlibrary:0.2.1'
```

## Implementation

To call Litmus Library, do below steps -

### Initiate a conversation in Full Screen

```ruby
import com.litmusworld.litmuscxlibrary.activities.LitmusRatingActivity;
```

#### Open Conversation from App id

```ruby
 LitmusRatingActivity.fnStartActivity(strUserId, strAppId, strUserName, nReminderNumber,
                    strUserEmail, isAllowMultipleFeedbacks, strBaseUrl, tagParameters, context, 
                    isCopyLinkAllowed, isShareLinkAllowed, isMoreImageBlackElseWhite);


```

#### Open Conversation from URL

```ruby
LitmusRatingActivity.fnOpenWebLinkActivity(previewUrl, context, isCopyLinkAllowed, isShareLinkAllowed, isMoreImageBlackElseWhite);
```

### Initiate a conversation in a Dialog

```ruby
import com.litmusworld.litmuscxlibrary.fragments.dialog.LitmusRatingDialogFragment;
```

#### Open Conversation from App id

```ruby
LitmusRatingDialogFragment.newInstanceAndShow(strBaseUrl, strUserId, strAppId, strUserName,
                                                nReminderNumber, strUserEmail, isAllowMultipleFeedbacks, tagParameters,
                                                fragmentManager, isCopyLinkAllowed, isShareLinkAllowed, isMoreImageBlackElseWhite);

```

####  Open Conversation from URL

```ruby
LitmusRatingDialogFragment.newInstanceAndShow(strWebUrl, fragmentManager, isCopyAllowed, isShareAllowed, isMoreImageBlackElseWhite)
```

Where -

strUserId = User Id (Required)

strAppId = Application feedback Id (To be provided by litmus)

strUserName = User Name (Optional)

nReminderNumber = Reminder Number (-1 will call Api every time)

strUserEmail = Email Id of the user (Optional)

isAllowMultipleFeedbacks = true if multiple feedback is given by same user, false otherwise

strBaseUrl = Base url to be used to get conversation url (Optional) (Default if not sent is "https://app.litmusworld.com/rateus", other values are For Demo = "https://demo.litmusworld.com" For App india = "https://app-india.litmusworld.com/rateus"

tagParameters = Dictionary with key-value pairs to be sent with above conversation (Optional)

context = Context instance

strWebUrl = URL link to be opened in Webview

isCopyLinkAllowed = true if Copy option is enabled in more option, false otherwise

isShareLinkAllowed = true if Share option is enabled in more option, false otherwise

isMoreImageBlackElseWhite = true if you want More Image (Image with 3 dots) as Black in color, false in case of White color

fragmentManager = Support Fragment manager instance

## Author

Fenil15, fenil.jain@litmusworld.com , avinash.pathare@litmmusworld.com

## License

LitmusCXLibrary is available under the Apache License, Version 2.0. See the LICENSE file for more info.